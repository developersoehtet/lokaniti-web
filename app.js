const express = require('express');
const mongoose = require('mongoose');

const path = require('path');

const exphbs = require('express-handlebars');




global.Language = 'MM';

const app = express();


// Handlebars Middlewares
app.engine('handlebars', exphbs(
  {
    defaultLayout: 'main'
  }
));
app.set('view engine', 'handlebars');

// Load routes
const index = require('./routes/index');



//set static folder
app.use(express.static(path.join(__dirname,'public')))

//Use routes
app.use('/',index);

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log('Server started on port ' + port);
});
