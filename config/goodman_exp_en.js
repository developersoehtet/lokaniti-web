module.exports=[
        {'title':'1','content':'A good and virtuous person is honest and right. He avoids ill deads. Associate with good men and learn from them the Law (Dhamma). Such knowledge is sacred, not debased.'},
        {'title':'1','content':'Go along with the virtuous; shun the wicked. Do good deads continuously every day, Study always the impermanence of all things.'},
        {'title':'1','content':'The wicked are apt to pose as the virtuous. They are very tickly. Their word are sweet, but their hearts are evil.'},
        {'title':'1','content':'The virtuous persons are honest. They are not like the wicked who put up appearances; they have goos hearts.'},
        {'title':'1','content':'Some things have intrinsic qualities, and if they are good qualities the things or persons who possess them are regarded as distinct. The virtuous person, though lacking wealth, never resorts to ill deeds, and walks alongs the right path.'},
        {'title':'1','content':'Lion does not eat leaves though starving : It may be lean from starvation, but it does not eat elephant flesh.'},
        {'title':'1','content':'Man of noble birth does not do any mean deed in order to keep up the dignity of his lineage.'},
        {'title':'1','content':'The scent of the sandalwood and the light of the moon are resptively pleasant, The speech of the virtuous is much more pleasant. It reduces stressful suffering and render happiness.'},
        {'title':'1','content':'Natural phenomena may change,but the world of the virtuous will remains unchanged. It will be good for all time.'},
        {'title':'1','content':'"Of the various shades and shelters that of the Dhamma, Buddha\'s word, is the most pleasant. Something has to said of the respective shades. Those of relatives, teachers, and king may give comfort in accordance with the nature of the individual concerned. The shades and shelters that the uncheritable relatives, teachers and king offer cannot possibly by pleasant. Only the love of parents, and the Dhamma which Buddhas taught in His great compassion can make for the shelter-seeker\'s happiness."'},
        {'title':'1','content':'Noble persons wish for what is virtuous. The wicked and the lowly enjoy anger, the evil state of mind.'},
        {'title':'1','content':'"There is a saying to the effect that the son of a wicked man is rude, whereas the son of a wise man is polite. At the beginning of life, children learn everything from parents, Wicked parent\'s children are seldom wise."'},
        {'title':'1','content':'Children follow their parents\'s tradition. Those of bad parents have wicked character, and those of virtuous parents are well-behaved.'},
        {'title':'1','content':'In various activities, persons who can give help and guidance are required to be present. If they are absent, undesirable incidents can occur.'},
        {'title':'1','content':''},
        {'title':'1','content':'You should do things after due consideration. You should not do anythings impulsively, nor should you make others do hastily. Or you may suffer later and may have to repent.'},
        {'title':'1','content':'Anger is so savage that it is difficult to curb. The person who can curb his anger does not have to worry. The one who knows gratitude is called noble by virtuous people.'},
        {'title':'1','content':'An ungrateful wretch is the most loathsome one. Such a person is wicked and mean. He does not find it difficult to commit any kind of sin. He does not hesitate to murder his benefactor.'},
        {'title':'1','content':'Admonish where admonition is called for. Deter wicked action. That is what it should be. The person who can give such guidance to others is loved by the good people and hated by the bad.'},
        {'title':'1','content':'The one superior to him, the one inferior, the one of equal status, and the brave one: all these persons can be overcome by various ways of approach. Thus you will be the winner and get the benefit.'},
        {'title':'1','content':'"Ordinary poison kills many only once.The poison which is a monk\'s possessions kills him many times ( in the course of the cycle of existence)."'},
        {'title':'1','content':'The world of anything or any person can be determined by its capacity or his ability. Evaluation should be made by scanning the special quality concerned.'},
        {'title':'1','content':'The virtuous are charitable, so they give away what little they have. The non-virtuous persons are selfish, so they are reluctant to give away anything although they have much, just like the water in the ocean which cannot enjoyed by anyone.'},
        {'title':'1','content':'Just as rivers do not drink their water, trees do not eat their fruit, rain does not fall in all places, so also the virtuous do not enjoy their wealth all by themselves. They give it in charity for the benefit of others.'},
        {'title':'1','content':'"Do not long for things that cannot be obtained. Do not contemplate what cannot be thought up, thus making your brain  run dry. Do not waste your time. Time is precious."'},
        {'title':'1','content':'Thought not planned and tried, things happen by themselves. Plans made and tried do not produce the desired results. Things are apt to go away. That is natural in this world.'},
        {'title':'1','content':'The person who associate with the wicked and listens to the words of the vicious, will go to ruin.'},
]
