const sage_en = require('./config/sage_en');
const sage_mm = require('./config/sage_mm');
const sage_exp_en = require('./config/sage_exp_en');
const sage_exp_mm = require('./config/sage_exp_mm');

var tmp_arr = []

for (var i = 0; i < 38; i++) {
  var tmp = {
    'title': sage_en[i].title,
    'content': sage_en[i].content + '\n\nExplanation: :'+ sage_exp_en[i].content
  }
  tmp_arr.push(tmp)
}

console.log("******************")
console.log("******************")
console.log(tmp_arr);
console.log("******************")
console.log("******************")
