const express = require('express');
const router = express.Router();

const title = require('./../config/title');
const title_en = require('./../config/title_en');
const title_mm = require('./../config/title_mm');

const sage_en = require('./../config/sage_en');
const sage_mm = require('./../config/sage_mm');
const goodman_en = require('./../config/goodman_en');
const goodman_mm = require('./../config/goodman_mm');
const badman_en = require('./../config/badman_en');
const badman_mm = require('./../config/badman_mm');
const friend_en = require('./../config/friend_en');
const friend_mm = require('./../config/friend_mm');
const women_en = require('./../config/women_en');
const women_mm = require('./../config/women_mm');
const king_en = require('./../config/king_en');
const king_mm = require('./../config/king_mm');
const mil_en = require('./../config/mil_en');
const mil_mm = require('./../config/mil_mm');

router.get('/', (req, res) => {
  var contents = [];
  for (var i = 0; i < 20; i++) {
    tmp = {'test':'test'};
    contents.push(tmp);
  }
  const logo = global.Language === 'EN' ? title_en.logo:title_mm.logo;
  const part_1 = global.Language === 'EN' ? title_en.part_1:title_mm.part_1;
  const part_2 = global.Language === 'EN' ? title_en.part_2:title_mm.part_2;
  const part_3 = global.Language === 'EN' ? title_en.part_3:title_mm.part_3;
  const part_4 = global.Language === 'EN' ? title_en.part_4:title_mm.part_4;
  const part_5 = global.Language === 'EN' ? title_en.part_5:title_mm.part_5;
  const part_6 = global.Language === 'EN' ? title_en.part_6:title_mm.part_6;
  const part_7 = global.Language === 'EN' ? title_en.part_7:title_mm.part_7;

  const sage_man = global.Language === 'EN' ? sage_en:sage_mm;
  const good_man = global.Language === 'EN' ? goodman_en:goodman_mm;
  const bad_man = global.Language === 'EN' ? badman_en:badman_mm;
  const friend = global.Language === 'EN' ? friend_en:friend_mm;
  const women = global.Language === 'EN' ? women_en:women_mm;
  const king = global.Language === 'EN' ? king_en:king_mm;
  const mil = global.Language === 'EN' ? mil_en:mil_mm;
  res.render('index/welcome',
    {
      'contents': contents, title,
      'logo': logo,
      'Language': global.Language,
      'part_1': part_1,
      'part_2': part_2,
      'part_3': part_3,
      'part_4': part_4,
      'part_5': part_5,
      'part_6': part_6,
      'part_7': part_7,
      'sage':sage_man,
      'good_man': good_man,
      'bad_man':bad_man,
      'friend':friend,
      'women':women,
      'king':king,
      'mil':mil,
    });
})

router.get('/:ln', (req, res) => {
  var contents = [];
  global.Language = req.params.ln
  for (var i = 0; i < 20; i++) {
    tmp = {'test':'test'};
    contents.push(tmp);
  }

  const logo = global.Language === 'EN' ? title_en.logo:title_mm.logo;
  const part_1 = global.Language === 'EN' ? title_en.part_1:title_mm.part_1;
  const part_2 = global.Language === 'EN' ? title_en.part_2:title_mm.part_2;
  const part_3 = global.Language === 'EN' ? title_en.part_3:title_mm.part_3;
  const part_4 = global.Language === 'EN' ? title_en.part_4:title_mm.part_4;
  const part_5 = global.Language === 'EN' ? title_en.part_5:title_mm.part_5;
  const part_6 = global.Language === 'EN' ? title_en.part_6:title_mm.part_6;
  const part_7 = global.Language === 'EN' ? title_en.part_7:title_mm.part_7;

  const sage_man = global.Language === 'EN' ? sage_en:sage_mm;
  const good_man = global.Language === 'EN' ? goodman_en:goodman_mm;
  const bad_man = global.Language === 'EN' ? badman_en:badman_mm;
  const friend = global.Language === 'EN' ? friend_en:friend_mm;
  const women = global.Language === 'EN' ? women_en:women_mm;
  const king = global.Language === 'EN' ? king_en:king_mm;
  const mil = global.Language === 'EN' ? mil_en:mil_mm;
  res.render('index/welcome',
    {
      'contents': contents,
      'logo': logo,
      'Language': global.Language,
      'part_1': part_1,
      'part_2': part_2,
      'part_3': part_3,
      'part_4': part_4,
      'part_5': part_5,
      'part_6': part_6,
      'part_7': part_7,
      'sage':sage_man,
      'good_man': good_man,
      'bad_man': bad_man,
      'friend':friend,
      'women':women,
      'king':king,
      'mil':mil
    });
})



router.get('/1', (req, res) => {
  var contents = [];
  for (var i = 0; i < 20; i++) {
    tmp = {'test':'test'};
    contents.push(tmp);
  }
  res.render('index/test', {'contents': contents});
})
router.get('/2', (req, res) => {
  var contents = [];
  for (var i = 0; i < 20; i++) {
    tmp = {'test':'test'};
    contents.push(tmp);
  }
  res.render('index/test2', {'contents': contents});
})
router.get('/3', (req, res) => {
  var contents = [];
  for (var i = 0; i < 20; i++) {
    tmp = {'test':'test'};
    contents.push(tmp);
  }
  res.render('index/test3', {'contents': contents});
})

router.get('/dashboard', (req, res) => {
  res.send('dashbord');
})


module.exports = router;
